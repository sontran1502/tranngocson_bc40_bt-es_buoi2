export default class Glasses{
    constructor (_id,_srcG,_srcV,_brand,_name,_color,_price,_description){
        this.id = _id;
        this.src = _srcG;
        this.virtualImg = _srcV;
        this.brand = _brand;
        this.name = _name;
        this.color = _color;
        this.price= _price;
        this.description = _description;
    }
}