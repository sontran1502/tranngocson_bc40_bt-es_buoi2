export let renderGlassesImg = (object) => { 
    let listImg= "";
    object.forEach((item) => { 
        let imgHtml = /*html*/ `
           <div onclick="thuKinh('${item.id}')" class ="col-4"><img class="block" src="${item.src}"/></div>
         `
         listImg+=imgHtml;
     })
     document.getElementById("vglassesList").innerHTML=listImg;
 }

 export let showThongTin = (item) => { 
    let Infor =/*html*/ `
    <h2>${item.name}</h2>
    <button class="btn btn-danger">${item.price}</button>
    <span class="text-success">${item.color}</span>
    <p>${item.description}</p>
     `
     document.getElementById("glassesInfo").innerHTML=Infor;
  }

export let deoKinh= (item) => { 
    let deoKinh = /*html*/ `
    <img class="deoKinh" src="${item.virtualImg}" alt="">
     `
     document.getElementById("avatar").innerHTML=deoKinh;
 }  